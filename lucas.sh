#!/bin/bash
#De to præ-definerede lucastal, l1 og l2. Bemærk, at det første tal er 2.
l1=2
l2=1
sum=0

#Der tages imod et argument, som er det, tallene skal sammenlignes imod.
argument=$1


#Tjekker om argumentet er lig med, eller under 0. Er dette tilfældet, stopper programmet.
if [[ "$argument" -le 0 ]]; then
	:
#Hvis det indtastede er 1, er det kun l2, altså 1, som er lavere.
elif [[ "$argument" -eq 1 ]]; then
	echo $l2
#Hvis det indtastede er 2, er l1 og l2 lavere, men der kan endnu ikke laves beregninger i loopet.
#Derfor printes de første to tal blot.
elif [[ "$argument" -eq 2 ]]; then
	echo $l1
	echo $l2
#Hvis de forrige ikke er opfyldt må tallet være > 2, og vi kan nu regne lucastallene via et while loop.
else
	#Ligesom før, kan de to første værdier ikke ingå i loopet, og skal manuelt printes.
	echo $l1
	echo $l2
	#mens summen af de to lucastal er lavere end argumentet laver vi operationerne for lucastallene. 
	#Her bruges operatoren -le, som er "less than, or equal".
	while [[ "$sum" -le "$argument" ]]
	do
		#summen af de forgænge to tal sammen skrives i konsollen.
		sum=$(($l1 + $l2))
		echo $sum

		#Dernæst forøges det første tal (det laveste) til det andet tal (det højeste)
		l1=$l2
		#Det andet tal forøges til summen af de forgænge tal.
		l2=$sum

		#Summen opdateres udfra de to nye værdier.
		sum=$(($l1 + $l2))
	done
fi